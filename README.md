# singularity-gnome-theme
Repository for next Singularity Gnome theming

PKGBUILD [here](https://gitlab.manjaro.org/packages/community/gnome/singularity-gdm-theme)

Wallpaper by Muser forum@manjaro.org

<p align="center">
  <img alt="GDM Wallppaer" src="https://github.com/Ste74/singularity-gnome-theme/blob/master/theme/noise-texture.png" alt="alt text" width="800" height="452">
</p>
